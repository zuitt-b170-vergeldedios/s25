{ $count: "fruits on sale" }

db.scores.aggregate(
  [
    {
      $match: {
        score: {
          $gt: 20
        }
      }
    },
    {
      $count: "total number of fruits"
    }
  ]
)


db.sales.aggregate(
   [
     {
       $group:
         {
           _id: "$fruitsonsale",
           avgAmount: { $avg: { $multiply: [ "$price", "$quantity" ] } },
           avgQuantity: { $avg: "$quantity" }
         }
     }
   ]
)

db.sales.aggregate(
   [
     {
       $group:
         {
           _id: "$fruits",
           maxTotalAmount: { $max: { $multiply: [ "$price", "$quantity" ] } },
           maxQuantity: { $max: "$quantity" }
         }
     }
   ]
)

db.sales.aggregate(
   [
     {
       $group:
         {
           _id: "fruits",
           minQuantity: { $min: "$quantity" }
         }
     }
   ]
)